<?php
/**
 * @file
 * Optimize images using Yahoo!'s Smush.it webservice.
 * http://www.smushit.com/ysmush.it/
 *
 * @author Jonathan Hunt imagecache_smushit.module@huntdesign.co.nz
 *
 * @todo:
 * watchdog truncates display/storage of 'type'
 *
 * Note each use of smushit involves two HTTP requests, one for JSON data, 2nd for image data.
 *
 * Requires patch for imagecache.module
-function _imagecache_apply_action($action, $image) {
+function _imagecache_apply_action($action, &$image) {
*/

/**
 * Implementation of hook_imagecache_actions().
 *
 * @return array
 *   An array of information on the actions implemented by a module.
 */
function imagecache_smushit_imagecache_actions() {
  $actions = array(
    'imagecache_smushit' => array(
      'name' => 'Smush.it',
      'description' => t("Optimize images using Yahoo!'s Smush.it webservice."),
    ),
  );

  return $actions;
}

/**
 * Send image to Smush.it webservice, retrieve json description, then retrieve image data.
 * @todo:
 * Log compression?
 * Smush.it returns 200 even when error occurs
 * [data] => {"src":"http:\/\/.../imagecache_sample.png","error":"Could not get the image","id":""}
 * Also, no savings
 * [data] => {"src":"http:\/\/...\/imagecache_sample.png","src_size":25329,"error":"No savings","dest_size":-1,"id":""}
 */
function imagecache_smushit_image(&$image, $data) {

  // Save outcome of smush in local files, there may be additional image ops.
  $dir = file_create_path('smushit');
  if (!file_check_directory($dir)) {
    mkdir($dir, 0775, FALSE);
  }
  $hash = md5($image->source);
  $cachepath = file_create_path('smushit/'. $hash) .'.'. $image->info['extension'];
  $original_filename = basename($image->source);

  // If image ops are queued, we need to invoke image toolkit to process those ops, then smush the resulting image.
  if (!empty($image->ops)) {
    // src=sites/example.com/files/imagecache_sample.png
    if (!imageapi_image_close($image, $cachepath)) {
      watchdog('imagecache_smushit', 'There was an error saving the new image file %cachepath.', array('%cachepath' => $cachepath), WATCHDOG_ERROR);
      return FALSE;
    }
    // We've processed the image, so work on the new image from now on.
    // Reopen the image for processing
    if (!$image = imageapi_image_open($cachepath)) {
      return FALSE;
    }
  }

  // Submit image to smush.it for processing.
  $source_url = url($image->source, array('absolute' => TRUE));
  $smushit_url = 'http://www.smushit.com/ysmush.it/ws.php?img='. $source_url;
  $response = drupal_http_request($smushit_url);

  if ($response->code == 200) {
    $data = json_decode($response->data);
    if (empty($data->error)) {
      // Could log savings?
      $smushed_image_url = $data->dest;
      $image_response = drupal_http_request($smushed_image_url);

      // Code is based on imagecache_external.module
      $code = floor($image_response->code / 100) * 100;
      $types  = array('image/jpeg', 'image/png', 'image/gif');
      if (!empty($image_response->data) && $code != 400 && $code != 500 && in_array($image_response->Content-Type, $types)) {
         // Smush.it was successful. Retrieve image, write to filesystem,
         // pass file path as new image source for further ops.
         // overwrite our cached image...
         $source = file_save_data($image_response->data, $cachepath, FILE_EXISTS_REPLACE); 
         watchdog('imagecache_smushit', t('@percent saving for %image', array('@percent' => $data->percent, '%image' => basename($original_filename))));
         // @todo: Write may have failed. Check.
         $image->source = $source;
         $image->info['file_size'] = filesize($source);
      }
      else  {
        //if we are unsuccessful then log a message in watchdog
        watchdog('imagecache_smushit', 'The image %url could not be retrieved', array('%url' => $url));
        return drupal_not_found();
      }
    }
    else {
      if ($data->error == 'Could not get the image') {
        watchdog('imagecache_smushit', 'Smush.it could not locate the image %source, check paths or access control.', array('%source' => $image->source), WATCHDOG_ERROR);
      }
      if ($data->error == 'No savings') {
        watchdog('imagecache_smushit', 'Smush.it reports no savings found for image %source.', array('%source' => $image->source), WATCHDOG_NOTICE);
      }
    }
  }
  else {
    watchdog('imagecache_smushit', 'Smush.it returned @code processing %source', array(
      '@code' => $response->code,
      '%source' => $image->source,
    ), WATCHDOG_ERROR);
  }

  return TRUE;
}